# Few shot learning with GNNs

investigate the effectiveness of using GNN's node embeddings for fine-tuning pretrained language model, 
such that it requires less training data.

### config
the centralized place for storing configurations for experiments, as well as a validator to validate the config schema.

### models
where the models are implemented.

### parsers
for cmd argument parser


### sacred
an experiment logger, which provides a neat way to observer the training on the fly via a web frontend (e.g., sacredboard, omniboard).
